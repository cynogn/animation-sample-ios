//
//  IAViewController.m
//  Animations
//
//  Created by Gautam B on 23/08/13.
//  Copyright (c) 2013 Gautam B. All rights reserved.
//

#import "IAViewController.h"

@interface IAViewController ()
@property (weak, nonatomic) IBOutlet UILabel *Button;
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;
@property (weak, nonatomic) IBOutlet UILabel *readMoreNews;
@property (weak, nonatomic) IBOutlet UIButton *ImageButton;

@end

@implementation IAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated{
    

    
    
    [UIView animateWithDuration:.5
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.Button.transform = CGAffineTransformMakeTranslation(0, self.Button.frame.size.height+20);
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:1
                                               delay:0
                                             options: UIViewAnimationCurveLinear
                                          animations:^{
                                             
                                              self.ImageButton.transform = CGAffineTransformMakeTranslation(0, - self.ImageButton.frame.size.height);
                                              self.newsTitle.transform = CGAffineTransformMakeTranslation(0, -self.ImageButton.frame.size.height);
                                              
                                          }
                                          completion:^(BOOL finished){

                                              [UIView animateWithDuration:1
                                                                    delay:0
                                                                  options: UIViewAnimationOptionCurveLinear
                                                               animations:^{
                                                                   self.readMoreNews.transform = CGAffineTransformMakeTranslation( self.newsTitle.frame.origin.x+self.newsTitle.frame.size.width,0);
                                                               }
                                                               completion:^(BOOL finished){
                                                                 }
                                               
                                               ];
                                               

                                              
                                               }];
                     }];
    
}

@end
