//
//  IAAppDelegate.h
//  Animations
//
//  Created by Gautam B on 23/08/13.
//  Copyright (c) 2013 Gautam B. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IAViewController;

@interface IAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IAViewController *viewController;

@end
