//
//  IAViewController.h
//  Animations
//
//  Created by Gautam B on 23/08/13.
//  Copyright (c) 2013 Gautam B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IAViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *WisdomOftheDay;

@end
