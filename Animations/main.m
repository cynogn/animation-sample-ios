//
//  main.m
//  Animations
//
//  Created by Gautam B on 23/08/13.
//  Copyright (c) 2013 Gautam B. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IAAppDelegate class]));
    }
}
